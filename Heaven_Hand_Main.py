# -*- coding: utf-8 -*-
from datetime import datetime
import io
import sys
import pandas as pd
import akshare as ak
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QColor, QIcon
from PyQt5.QtWidgets import QMainWindow, QApplication, QTableWidgetItem, QLabel, QHeaderView, \
    QMessageBox, QSizePolicy

from forms.mainWindow import Ui_MainWindow
from utils.KGraph import PyQtGraphLineWidget
from utils.ThreadUpdateWidget import ThreadUpdateWidget, ThreadUpdateZhWidget

# 1 get_stock_zt_pool_strong_data 强势股池
# 2 stock_zt_pool_em 涨停板
# 4 stock_hot_rank_em 东方财富-个股人气榜-人气榜
stock_type = 2

# 主显示界面勾画
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.searchRow = None
        self.cur_row = 0
        self.cur_row2 = 0
        self.statusLabel = QLabel()
        self.widgetGraph = PyQtGraphLineWidget()  # 第一页的进度条之类
        self.timerUpdateWindow = QTimer()
        self.timerTableWidgetStocks = QTimer()  # 定时刷新第一页的股票列表
        self.setupUi(self)
        self.threadUpdateWidget = ThreadUpdateWidget(self)  # 第二页的自选股等
        self.threadUpdateZhWidget = ThreadUpdateZhWidget(self)  # 第二页的上证指数刷新
        self.initConnection()  # 槽函数，更新

        self.scrollBar = self.tableWidgetStocks.verticalScrollBar()

        self.btnSelectStock.click()  # 第一页的查询按钮
        self.timerUpdateWindow.start()  # 定时3秒更新第二页上证指数
        self.timerTableWidgetStocks.stop()  # 定时5秒更新第一

    def setupUi(self, MainWindow):
        super().setupUi(self)
        self.progressBar.hide()
        self.setMinimumSize(1920, 960)  # 固定窗口大小
        self.setMaximumSize(1920, 960)
        self.setWindowIcon(QIcon("./icons/stock2.ico"))

        # 右下角状态栏显示文字
        self.statusbar.addPermanentWidget(self.statusLabel)
        self.widgetGraph.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.tabWidget.setCurrentIndex(0)
        self.tabWidget.setTabText(0, '行情数据')
        self.tabWidget.setTabText(1, '自选股票')
        self.tabWidget.setTabText(2, '整体格局')
        self.tabWidget.setTabText(3, '其他行情')
        self.timerUpdateWindow.setInterval(3000)  # 定时器时间间隔
        self.timerTableWidgetStocks.setInterval(10000)

    def initConnection(self):
        self.threadUpdateWidget.signalSetStockWidget.connect(self.slotSetStockWidget)
        self.threadUpdateWidget.signalAddStockToWidget.connect(self.slotAddStockToWidget)
        self.threadUpdateWidget.signalUpdateProgressBar.connect(self.progressBar.setValue)
        self.threadUpdateWidget.signalShowPermMessage.connect(self.statusLabel.setText)
        self.threadUpdateWidget.signalWidgetRowChangeColor.connect(self.slotSetStockWidgetRowColor)
        self.threadUpdateWidget.finished.connect(self.on_threadUpdateWidget_finished)
        self.timerUpdateWindow.timeout.connect(self.threadUpdateZhWidget.start)  # 定时3秒刷新上证指数
        self.timerTableWidgetStocks.timeout.connect(self.threadUpdateWidget.start)  # 定时5秒刷新股票数据
        self.threadUpdateZhWidget.signalUpdateWindow.connect(self.slot_update_window)
        self.tableWidgetStocks.horizontalHeader().sectionClicked.connect(self.HorSectionClicked)  # 表头单击信号
        # self.timerTableWidgetStocks.deleteLater()
        self.cbArranges.currentIndexChanged.connect(self.cbArranges_currentIndexChanged)
        self.comboBox.currentIndexChanged.connect(self.comboBox_currentIndexChanged)
        self.tableWidgetSelfSelect_2.cellClicked.connect(self.tableWidgetSelfSelect_2_CellClicked)
    def btnSearchText_currentTextChanged(self):
        # Clear current selection.
        s = self.btnSearchText.toPlainText()
        self.tableWidgetStocks.setCurrentItem(None)
        print('s2:', s)
        if not s:
            # Empty string, don't search.
            print('s:', s)
            return

        matching_items = self.tableWidgetStocks.findItems(s, Qt.MatchContains)
        if matching_items:
            # We have found something.
            item = matching_items[0]  # Take the first.
            self.tableWidgetStocks.setCurrentItem(item)
        pass

    def get_stock_zt_pool_data(self):
        now_day = datetime.today()
        str_now = str(now_day.date())
        str_now = str_now.replace('-', '')
        stock_hot_rank_latest_em_df = ak.stock_zt_pool_em(date='20221202')
        self.stock_df = stock_hot_rank_latest_em_df.sort_values(by='连板数', ascending=[False])
        self.m, self.n = self.stock_df.shape
        return self.stock_df

    def get_stock_zt_pool_strong_data(self):
        now_day = datetime.today()
        str_now = str(now_day.date())
        str_now = str_now.replace('-', '')
        print(str_now)
        #获取东方财富网-行情中心-涨停板行情-强势股池
        self.strong_df = ak.stock_zt_pool_strong_em(date='20221209')
        self.stock_em_zt_pool_strong_df = self.strong_df.loc[:, ['序号', '代码', '名称', '涨跌幅', '最新价', '涨停价',
                                                                 '换手率', '涨速', '是否新高', '量比', '涨停统计',
                                                                 '入选理由', '所属行业']]
        self.m, self.n = self.stock_em_zt_pool_strong_df.shape
        self.stock_df = self.stock_em_zt_pool_strong_df.sort_values(by='涨停统计', ascending=[False]).copy()
        return self.stock_df

    def get_stock_sina_lhb_ggtj_data(self):
        self.tock_sina_lhb_ggtj_df = ak.stock_sina_lhb_ggtj(recent_day="5")
        self.m, self.n = self.tock_sina_lhb_ggtj_df.shape
        self.stock_df = self.tock_sina_lhb_ggtj_df.sort_values(by='买入席位数', ascending=[False]).copy()
        return self.stock_df

    def get_stock_hot_rank_em_data(self):
        self.strong_df = ak.stock_hot_rank_em()
        self.m, self.n = self.strong_df.shape
        self.stock_df = self.strong_df.sort_values(by='当前排名', ascending=[True]).copy()
        return self.stock_df
    def get_stock_rank_cxg_ths(self, s_type):
        if s_type == 5:
            s_type_symbol = "创月新高"
        elif s_type == 6:
            s_type_symbol = "半年新高"
        elif s_type == 7:
            s_type_symbol = "一年新高"
        elif s_type == 8:
            s_type_symbol = "历史新高"
        else:
            s_type_symbol = "历史新高"
        self.strong_df = ak.stock_rank_cxg_ths(symbol=s_type_symbol)
        self.m, self.n = self.strong_df.shape
        print('get_stock_rank_cxg_ths:self.m:', self.m, ',self.n:', self.n)
        self.stock_df = self.strong_df.copy()
        return self.stock_df

    def btnRemoveSelfSelect_2_showlist(self):
        global stock_type
        stock_type = self.comboBox.currentIndex()
        self.upate_tableContent()
        pass
    def btnSearchBtn_search(self):
        # Clear current selection.
        s = self.btnSearchText.toPlainText()
        self.tableWidgetStocks.setCurrentItem(None)
        print('s2:', s)
        if not s:
            # Empty string, don't search.
            print('s:', s)
            return

        if self.searchRow:
            for i in range(21):
                self.tableWidgetStocks.item(self.searchRow, i).setBackground(QColor('white'))
                if i == 3:
                    text = self.tableWidgetStocks.item(self.searchRow, i).text()
                    if float(text) < 0:
                        # 改变单元格颜色:
                        self.tableWidgetStocks.item(self.searchRow, i).setBackground(QColor('gray'))
                        self.tableWidgetStocks.item(self.searchRow, i - 1).setBackground(QColor('gray'))
                    else:
                        if float(text) >= 5:
                            self.tableWidgetStocks.item(self.searchRow, i).setBackground(QColor('red'))
                            self.tableWidgetStocks.item(self.searchRow, i - 1).setBackground(QColor('red'))
                        else:
                            self.tableWidgetStocks.item(self.searchRow, i).setBackground(QColor('sienna'))
                            self.tableWidgetStocks.item(self.searchRow, i - 1).setBackground(QColor('sienna'))

        matching_items = self.tableWidgetStocks.findItems(s, Qt.MatchContains)
        if matching_items:
            # We have found something.
            item = matching_items[0]  # Take the first.
            row = item.row()
            self.searchRow = row
            for i in range(21):
                self.tableWidgetStocks.item(row, i).setBackground(QColor('blue'))
        pass
    def cbArranges_currentIndexChanged(self, cur_i: int):
        self.threadUpdateWidget.setData(cur_i)
        self.threadUpdateWidget.start()
        pass

    def upate_tableContent(self, s_type):
        self.tableWidgetSelfSelect_2.clearContents()
        if s_type == 1:
            df_data = self.get_stock_zt_pool_strong_data()
        elif s_type == 2:
            df_data = self.get_stock_zt_pool_data()
        elif s_type == 3:
            df_data = self.get_stock_sina_lhb_ggtj_data()
        elif s_type == 4:
            df_data = self.get_stock_hot_rank_em_data()
        elif s_type == 5 or s_type == 6 or s_type == 7 or s_type == 8:
            df_data = self.get_stock_rank_cxg_ths(s_type)
        self.tableWidgetSelfSelect_2.setRowCount(self.m)
        self.tableWidgetSelfSelect_2.setColumnCount(self.n)
        self.tableWidgetSelfSelect_2.setHorizontalHeaderLabels(df_data.columns.tolist())
        print('upate_tableContent:self.m:', self.m, ',self.n:', self.n, ',s_type:', s_type)
        for i in range(self.m):
            for j in range(self.n):
                if s_type == 1:
                    if j == 3 or j == 4 or j == 5 or j == 6 or j == 7 or j == 8 or j == 9 or j == 11:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(
                            str(round(float(df_data.values[i][j]), 2))))
                    else:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(str(df_data.values[i][j])))
                elif s_type == 2:
                    if j == 0 or j == 1 or j == 2 or j == 5 or j == 6 or j == 7 or j == 9 or j == 13 or j == 15:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(str(df_data.values[i][j])))
                    else:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(
                            str(round(float(df_data.values[i][j]), 2))))
                elif s_type == 3:
                    if j == 0 or j == 1 or j == 2 or j == 5 or j == 6 or j == 7 or j == 9 or j == 13 or j == 15:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(str(df_data.values[i][j])))
                    else:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(
                            str(round(float(df_data.values[i][j]), 2))))
                elif s_type == 4:
                    if j == 0 or j == 1 or j == 2 or j == 5 or j == 6 or j == 7 or j == 9 or j == 13 or j == 15:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(str(df_data.values[i][j])))
                    else:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(
                            str(round(float(df_data.values[i][j]), 2))))
                elif s_type == 5 or s_type == 6 or s_type == 7 or s_type == 8:
                    if j == 0 or j == 1 or j == 2 or j == 7:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(str(df_data.values[i][j])))
                    else:
                        self.tableWidgetSelfSelect_2.setItem(i, j, QTableWidgetItem(
                            str(round(float(df_data.values[i][j]), 2))))
        self.tableWidgetSelfSelect_2.update()
    def comboBox_currentIndexChanged(self, cur_i: int):
        global stock_type
        stock_type = cur_i+1
        self.upate_tableContent(stock_type)
        pass
    # 表头单机事件
    def HorSectionClicked(self, index):
        self.tableWidgetStocks.sortItems(index, QtCore.Qt.DescendingOrder)

    # 获取数据完毕
    @QtCore.pyqtSlot()
    def on_threadUpdateWidget_finished(self):
        self.progressBar.hide()
        self.btnSelectStock.setEnabled(True)
        if self.threadUpdateWidget.update:
            self.statusbar.showMessage(
                self.tabWidget.tabText(0) + '获取完毕，共' + str(self.threadUpdateWidget.stockDF.shape[0]) + '条数据')

    # 第二页的上证指数红绿显示
    @QtCore.pyqtSlot(list)
    def slot_update_window(self, data):
        self.labelStockName.setText(data[0]['f14'])
        self.labelNum.setText(f"{data[0]['f4'] / 100:.2f}")
        self.labelFloat.setText(f"{data[0]['f3'] / 100:.2f}%")
        if data[0]['f4'] < 0:
            self.labelNum.setStyleSheet('color:green')
            self.labelPrice.setStyleSheet('color:green')
            self.labelFloat.setStyleSheet('color:green')
            self.labelPrice.setText(f"{data[0]['f2'] / 100:.2f}↓")
        else:
            self.labelNum.setStyleSheet('color:red')
            self.labelPrice.setStyleSheet('color:red')
            self.labelFloat.setStyleSheet('color:red')
            self.labelPrice.setText(f"{data[0]['f2'] / 100:.2f}↑")

        self.labelStockName_2.setText(data[1]['f14'])
        self.labelNum_2.setText(f"{data[1]['f4'] / 100:.2f}")
        self.labelFloat_2.setText(f"{data[1]['f3'] / 100:.2f}%")
        self.labelPrice_2.setText(str(data[1]['f2']))
        if data[1]['f4'] < 0:
            self.labelNum_2.setStyleSheet('color:green')
            self.labelPrice_2.setStyleSheet('color:green')
            self.labelFloat_2.setStyleSheet('color:green')
            self.labelPrice_2.setText(f"{data[1]['f2'] / 100:.2f}↓")
        else:
            self.labelNum_2.setStyleSheet('color:red')
            self.labelPrice_2.setStyleSheet('color:red')
            self.labelFloat_2.setStyleSheet('color:red')
            self.labelPrice_2.setText(f"{data[1]['f2'] / 100:.2f}↑")

    # 查询按钮，重新获取数据
    @QtCore.pyqtSlot()
    def on_btnSelectStock_clicked(self):
        if self.cbStocks.currentIndex() == 0:
            self.statusbar.showMessage('正在获取：' + self.cbStocks.currentText())

            # update = True代表从网上更新本地数据，由于是第一次查询所以需要更新本地缓存数据
            self.threadUpdateWidget.update = True
            # 当前页，每次查询会将当前页置为第一页
            self.threadUpdateWidget.curPage = 0

            # 显示进度条
            self.progressBar.show()
            self.progressBar.setValue(0)
            self.tableWidgetStocks.clearContents()
            self.btnSelectStock.setDisabled(True)

            # 开始一个线程，将数据依次显示到表格
            self.threadUpdateWidget.start()

    # 前一页显示
    @QtCore.pyqtSlot()
    def on_btnPrePage_clicked(self):
        currentIndex = self.tabWidget.currentIndex()
        if currentIndex == 0:
            if self.threadUpdateWidget.curPage > 0:
                # 如果正在更新数据，则中断原线程
                if self.threadUpdateWidget.isRunning():
                    self.threadUpdateWidget.requestInterruption()

                # 等待原线程关闭
                self.threadUpdateWidget.wait()

                # 清除表格数据，将当前页码减一
                self.tableWidgetStocks.clearContents()
                self.threadUpdateWidget.curPage -= 1
                # 不从网页下载，直接使用本地缓存
                self.threadUpdateWidget.update = False
                # 开启线程更新界面
                self.threadUpdateWidget.start()
        elif currentIndex == 2:
            if self.cur_row2 > 20:
                self.cur_row2 = self.cur_row2 - 20
            else:
                self.cur_row2 = 0
            self.scrollBar.setValue(self.cur_row2)

    # 下一页显示
    @QtCore.pyqtSlot()
    def on_btnNextPage_clicked(self):
        currentIndex = self.tabWidget.currentIndex()
        if currentIndex == 0:
            if self.threadUpdateWidget.curPage < self.threadUpdateWidget.maxPage - 1:
                if self.threadUpdateWidget.isRunning():
                    self.threadUpdateWidget.requestInterruption()

                self.threadUpdateWidget.wait()

                self.tableWidgetStocks.clearContents()
                self.threadUpdateWidget.curPage += 1
                self.threadUpdateWidget.update = False

                self.threadUpdateWidget.start()
        elif currentIndex == 2:
            if self.cur_row2 > self.m:
                self.cur_row2 = self.m
            else:
                self.cur_row2 = self.cur_row2 + 20
            self.scrollBar.setValue(self.cur_row2)

    def tableWidgetSelfSelect_2_CellClicked(self, i: int, j: int):
        # 获取当前行所代表的股票的代码和名称
        code = self.tableWidgetSelfSelect_2.item(i, 1).text()
        name = self.tableWidgetSelfSelect_2.item(i, 2).text()
        self.queryAndShowStockGraph(code, name)
    # 查询近一年的K线图
    def queryAndShowStockGraph(self, code: str, name: str = ""):
        self.widgetGraph.code = code
        self.widgetGraph.name = name
        # 通过akshare接口获取股票历史行情信息
        if self.widgetGraph.query_slot(code):
            # 将单选按钮选中近一年，因为默认查询近一年
            #self.widgetGraph.radioBtn1y.setChecked(True)
            self.statusbar.showMessage("查询成功！", 2000)
            self.widgetGraph.showMaximized()
            self.widgetGraph.show()
        else:
            QMessageBox.warning(self, '查询失败', '查询失败，请查看控制台报错信息！')

    # 单元格双击，调出来K线图
    @QtCore.pyqtSlot(int, int)
    def on_tableWidgetStocks_cellDoubleClicked(self, i: int, j: int):
        # 获取当前行所代表的股票的代码和名称
        code = self.tableWidgetStocks.item(i, 0).text()
        name = self.tableWidgetStocks.item(i, 1).text()
        self.queryAndShowStockGraph(code, name)

    @QtCore.pyqtSlot(int, int)
    def on_tableWidgetSelfSelect_cellDoubleClicked(self, i: int, j: int):
        code = self.tableWidgetSelfSelect.item(i, 1).text()
        name = self.tableWidgetSelfSelect.item(i, 2).text()
        self.queryAndShowStockGraph(code, name)

    # 加自选
    @QtCore.pyqtSlot()
    def on_btnAddSelfSelect_clicked(self):
        """
        用户点击加自选按钮
        :return:
        """
        row = self.tableWidgetStocks.currentRow()
        if row >= 0:
            # 获取用户选择的股票代码
            code = self.tableWidgetStocks.item(row, 0).text()
            # 如果该股票在自选表格中，则报错
            for i in range(self.tableWidgetSelfSelect.rowCount()):
                if self.tableWidgetSelfSelect.item(i, 0).text() == code:
                    QMessageBox.information(self, '重复添加', '您已添加过该股票！')
                    return

            # 要写入到自选表格的行号
            new_row = self.tableWidgetSelfSelect.rowCount()
            col_count = self.tableWidgetStocks.columnCount()
            # 自选表格行数加一
            self.tableWidgetSelfSelect.setRowCount(new_row + 1)
            self.tableWidgetSelfSelect.setColumnCount(col_count)

            if new_row == 0:
                # 设置自选表格的表头
                header_labels = [self.tableWidgetStocks.horizontalHeaderItem(i).text() for i in range(col_count)]
                self.tableWidgetSelfSelect.setHorizontalHeaderLabels(header_labels)

            # 将股票添加到自选表格
            for j in range(col_count):
                self.tableWidgetSelfSelect.setItem(new_row, j,
                                                   QTableWidgetItem(self.tableWidgetStocks.item(row, j).text()))
                self.tableWidgetStocks.item(row, j).setBackground(QBrush(QColor('#00ae9d')))

            # 将该股票添加到自选列表
            self.threadUpdateWidget.selfSelect.append(self.tableWidgetStocks.item(row, 0).text())
        #           print('j:', j, 'new_row:', new_row, 'row:', row, "self.tableWidgetStocks.item(row, 0).text():",
        #                 self.tableWidgetStocks.item(row, 0).text())
        #           Note = open('zixuan.txt', mode='a+')
        #           Note.write(self.tableWidgetStocks.item(row, 0).text() + '\n')  # \n 换行符
        #           Note.close()

        self.statusbar.showMessage('添加自选成功！', 2000)

    # 移除自选
    @QtCore.pyqtSlot()
    def on_btnRemoveSelfSelect_clicked(self):
        row = self.tableWidgetSelfSelect.currentRow()
        if row >= 0:
            code = self.tableWidgetSelfSelect.item(row, 0).text()
            name = self.tableWidgetSelfSelect.item(row, 1).text()
            # 从列表中删除
            self.threadUpdateWidget.selfSelect.remove(code)
            # 从自选列表中删除
            self.tableWidgetSelfSelect.removeRow(row)
            # 刷新股票表格，更新股票行颜色
            self.threadUpdateWidget.start()
            self.statusbar.showMessage(f'移除自选：代码：{code}，名称：{name}')

    # 把stock加入到显示框中
    @QtCore.pyqtSlot(int, int, str)
    def slotAddStockToWidget(self, i: int, j: int, text: str):
        # self.tableWidgetStocks.setItem(i, j, QTableWidgetItem(text))
        # 第3行 涨跌幅
        if j == 3:
            data = QTableWidgetItem()
            data.setData(QtCore.Qt.DisplayRole, round(float(text), 2))
            self.tableWidgetStocks.setItem(i, j, data)
            if float(text) < 0:
                # 改变单元格颜色:
                self.tableWidgetStocks.item(i, j).setBackground(QColor('gray'))
                self.tableWidgetStocks.item(i, j - 1).setBackground(QColor('gray'))
            else:
                if float(text) >= 5:
                    self.tableWidgetStocks.item(i, j).setBackground(QColor('red'))
                    self.tableWidgetStocks.item(i, j - 1).setBackground(QColor('red'))
                else:
                    self.tableWidgetStocks.item(i, j).setBackground(QColor('sienna'))
                    self.tableWidgetStocks.item(i, j - 1).setBackground(QColor('sienna'))
        else:
            # 第一行 名称
            if j == 1:
                self.tableWidgetStocks.setItem(i, j, QTableWidgetItem(text))  # 字符串
            # 第0行 代码
            elif j == 0:
                data = QTableWidgetItem()
                data.setData(QtCore.Qt.DisplayRole, "%06d" % int(text))  # 整数全部都是6位
                self.tableWidgetStocks.setItem(i, j, data)
            # 第6行 成交额，第16,17行 总市值，流通市值
            elif j == 6 or j == 16 or j == 17:
                data = QTableWidgetItem()
                data.setData(QtCore.Qt.DisplayRole, round(float(text) / 100000000, 2))  # 保留2位小数，保留到亿级别
                self.tableWidgetStocks.setItem(i, j, data)
            else:
                data = QTableWidgetItem()
                data.setData(QtCore.Qt.DisplayRole, round(float(text), 2))
                self.tableWidgetStocks.setItem(i, j, data)

    # 设置显示框的一些基本信息，自动伸缩等
    @QtCore.pyqtSlot(int, int, list)
    def slotSetStockWidget(self, row_count: int, col_count: int, labels: list):
        self.tableWidgetStocks.setRowCount(row_count)
        self.tableWidgetStocks.setColumnCount(col_count)
        self.tableWidgetStocks.setColumnWidth(3, 5000)
        self.tableWidgetStocks.setHorizontalHeaderLabels(labels)
        self.tableWidgetStocks.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.tableWidgetStocks.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        for i in range(0, 22):
            self.tableWidgetStocks.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)
        for i in range(0, 19):
            self.tableWidgetStocks.setRowHeight(i, 38)

    @QtCore.pyqtSlot(int, str)
    def slotSetStockWidgetRowColor(self, row, color):
        try:
            col_cnt = self.tableWidgetStocks.columnCount()
            brush = QBrush(QColor(color))
            for j in range(col_cnt):
                self.tableWidgetStocks.item(row, j).setBackground(brush)
        except AttributeError as e:
            print(e)


    def wheelEvent(self, event):
        angle = event.angleDelta() / 8  # 返回QPoint对象，为滚轮转过的数值，单位为1/8度
        angleY = angle.y()
        self.wheel_flag = True
        # 获取当前鼠标相对于view的位置
        if angleY > 0:
            self.on_btnPrePage_clicked()
        else:  # 滚轮下滚
            self.on_btnNextPage_clicked()
        self.update()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec_())
