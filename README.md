# 上帝之手股票分析软件

#### 介绍
上帝之手股票分析软件[Heaven_Hand_stock] 是我写的股票查看分析软件，Heaven_Hand_stock使用python股票抓取和可视化，使用pyqt绘制界面，调用Akshare接口实时获取股票信息。

仓库地址：[https://gitee.com/lawrence2012/Heaven_Hand_stock]

#### 前言

Python比较高效，可以实现一些量化交易。

#### 缘起

　　多年来一直学习Python，很想使用Python制作一款自己的股票查看分析的软件。

#### 主界面展示

按最新价排列

![首页](https://gitee.com/lawrence2012/Heaven_Hand_stock/raw/master/img/1.jpg)

也可以按照名称，最新价，涨跌幅等排列

#### K线查看

查看上市以后所有K线图

![首页](https://gitee.com/lawrence2012/Heaven_Hand_stock/raw/master/img/2.jpg)

可以选择最近一年，二年，三年，五年，十年进行具体展示

![首页](https://gitee.com/lawrence2012/Heaven_Hand_stock/raw/master/img/3.jpg)

主图指标可以选择布林线或者均线展示

![首页](https://gitee.com/lawrence2012/Heaven_Hand_stock/raw/master/img/4.jpg)

可以查看可能上涨区间，可能下跌区间，用于股票预测

![首页](https://gitee.com/lawrence2012/Heaven_Hand_stock/raw/master/img/5.jpg)

### 项目进度

#### 已完成功能

- 使用akshare获取股票数据
- 使用数据实现主界面功能
- 使用数据实现K线基本功能

#### 待完成功能

需求优先级由高到低排序

- 展示主界面需要时间，需要实现分页功能
- 重排也需要实现分页功能
- 增加股票预测或者回测功能

#### 环境准备

* 支持的系统版本：Windows 10以上 / Windows Server 2016以上 / Ubuntu 20.04 LTS以上
* 支持的Python版本：Python  3.9 / 3.10 64位（**推荐使用Python 3.9**）

#### 安装步骤

在[这里](https://gitee.com/lawrence2012/Heaven_Hand_stock)下载，解压后运行以下命令安装：

**Windows**

```
install.bat
```

**Ubuntu**

```
bash install.sh
```

**注意：requirements.txt中给出了这些依赖库的推荐安装版本。**

#### 使用指南

1. 主界面查看所有股票信息。

2. 在K线图界面可以查看具体哪一天的涨幅之类，也可以放大缩小界面、

3. 在K线图界面可以使用功能查看可能上涨，可能下跌，一定上涨的地方


#### 脚本运行

在该目录下打开CMD（按住Shift->点击鼠标右键->在此处打开命令窗口/PowerShell）后运行下列命令启动Heaven_Hand_stock：

```
python Heaven_Hand_Main.py
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 其他内容

* [获取帮助](https://gitee.com/lawrence2012/Heaven_Hand_stock/blob/master/SUPPORT.md)
* [社区行为准则](https://gitee.com/lawrence2012/Heaven_Hand_stock/blob/master/CODE_OF_CONDUCT.md)
* [Issue模板](https://gitee.com/lawrence2012/Heaven_Hand_stock/blob/master/issue.md)
  * [PR模板](https://gitee.com/lawrence2012/Heaven_Hand_stock/blob/master/PULL_REQUEST_TEMPLATE.md)


#### 版权说明

**LGPL3**